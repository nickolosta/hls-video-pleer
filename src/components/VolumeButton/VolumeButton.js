import React from 'react';
import './styles.scss';

class VolumeButton extends React.Component {
  constructor(props){
    super(props);
    this.volumeDrag = false;
  }

  componentDidMount() {
    document.addEventListener("mouseup", this._onMouseUp.bind(this));
    document.addEventListener("mousemove", this._onMouseMove.bind(this));
  }

  componentWillUnmount() {
    document.removeEventListener("mouseup", this._onMouseUp);
    document.removeEventListener("mousemove", this._onMouseMove);
  }

  updateVolume(x, vol) {
    const volume = this.volumeElement;
    let percentage;
    if (vol) {
      percentage = vol * 100;
    } else {
      const rect = volume.getBoundingClientRect();
      const position = x - rect.left;
      percentage = 100 * position / volume.offsetWidth;
    }

    if (percentage > 100) {
      percentage = 100;
    }
    if (percentage < 0) {
      percentage = 0;
    }
    this.props.updateVolume(percentage);
  }

  _onMouseDown(event){
    this.volumeDrag = true;
   // console.log(event.pageX);
    this.updateVolume(event.pageX);
  }
  _onMouseUp(event){
    if(this.volumeDrag){
      this.volumeDrag = false;
      this.updateVolume(event.pageX);
    }
  }
  _onMouseMove(event){
    if(this.volumeDrag) {
      this.updateVolume(event.pageX);
    }
  }
  render() {
    const volume = this.props.volume,
      buttonClass = volume == 0 ? "volume-button-container__button-off" : "volume-button-container__button",
      special = volume > 0 ? "special" : "",
      volumeWidth = volume+"%";

    return (
      <div className="volume-button-container">
        <button className={buttonClass}/>
        <div
          ref={(volumeElement) => {
            this.volumeElement = volumeElement;
          }}
          onMouseDown={this._onMouseDown.bind(this)}
          className={["volume-button-container__volume-container",special].join(' ')}>
          <div style={{width: volumeWidth}} className="volume-button-container__volume"/>
        </div>
      </div>
    )
  }
}

export default VolumeButton
