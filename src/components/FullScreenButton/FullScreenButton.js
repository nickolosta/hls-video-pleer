import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

class FullScreenButton extends React.Component {
  renderHide(){
    return (
      <button onClick={this.props._onClick} className="controls-container__window-hide"/>
    )
  }
  renderFullScreen(){
    return (
      <button onClick={this.props._onClick} className="controls-container__window"/>
    )
  }
  render() {
    return !this.props.fullscreen ? this.renderFullScreen() : this.renderHide()
  }
}

FullScreenButton.propTypes = {
  _onClick: PropTypes.func,
  fullscreen: PropTypes.bool
};


export default FullScreenButton
