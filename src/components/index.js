import PlayButton from './PlayButton/PlayButton'
import VolumeButton from './VolumeButton/VolumeButton'
import FullScreenButton from './FullScreenButton/FullScreenButton'
import Time from './Time/Time'

export {
  PlayButton,
  VolumeButton,
  FullScreenButton,
  Time
}