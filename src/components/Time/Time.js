import React from 'react';
import './styles.scss';

class Time extends React.Component {
  render() {
    return (
      <div className="time-container">
        {this.props.value}
      </div>
    )
  }
}

export default Time
