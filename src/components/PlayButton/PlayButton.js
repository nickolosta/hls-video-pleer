
import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

class PlayButton extends React.Component {
  renderPause(){
    return (
      <button onClick={this.props._onClick} className="controls-container__play"/>
    )
  }
  renderPlay(){
    return (
      <button onClick={this.props._onClick} className="controls-container__pause"/>
    )
  }
  render() {
    return !this.props.play ? this.renderPlay() : this.renderPause()
  }
}

PlayButton.propTypes = {
  _onClick: PropTypes.func,
  play: PropTypes.bool
};

export default PlayButton
