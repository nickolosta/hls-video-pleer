import {connect} from 'react-redux'
import React from 'react'
import './styles.scss'
// containers
import {}  from '../../../containers'
// components
import {FullScreenButton, PlayButton, VolumeButton, Time}  from '../../../components'
import {videoAction, videoGetData} from '../../../redux/actions'
import ReactDOM from 'react-dom'
import {getWindowSize, toggleFullScreen, getTimeFromSeconds} from '../../../lib/Utilities'
import Hls from 'hls.js'

class Player extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 0,
      width: 0,
      hidePanels: true,
    };
    this.timer = null;
    this.hls_loading = false;
    this.hls_buffered = false;
  }

  /** Life cycles **/
  componentWillMount(){
    this.props.getData();
  }
  componentDidMount() {
    window.addEventListener("resize", this.getVideoSize.bind(this));
    document.addEventListener("mousemove", this._onMouseMove.bind(this));
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.getVideoSize.bind(this));
    document.removeEventListener("mousemove", this._onMouseMove);
  }

  componentDidUpdate() {
    const video = this.video;
    if(!this.state.hidePanels){
        if (!this.props.play && !video.paused) {
          console.log('pause!');
          video.pause();
        }
        if (this.props.play && video.paused) {
          console.log('play!');
          video.play();
        }
        video.volume = this.props.volume/100;
    }
    if(this.props.data != null && !this.hls_loading) {
      this.loadHLSPlugin();
    }
  }

  /** Life cycles [END] **/

  loadHLSPlugin(){
    // loading hls plugin
    const video = this.video;
      if(Hls.isSupported()) {
        const hls = new Hls();
        console.log("hls plugin loading");
        hls.loadSource(this.props.data.url);
        hls.attachMedia(video);
        this.hls_loading = true;
        hls.on(Hls.Events.MANIFEST_PARSED,function() {
          //video.play();
          this.hls_buffered = true;
          console.log("hls buffered");
          const {width, height} = getWindowSize();
          this.setState({width, height, hidePanels: false})
        }.bind(this));
      }else{
				alert("Hls видео не поддерживается этим браузером");
			}
  }

  getVideoSize() {
    const {width, height} = getWindowSize();
    this.setState({width, height});
  }

  /** Actions/events **/
  playOrStop() {
    this.props.setVideoState({variable: "play", value: !this.props.play});
  }

  videoFinished() {
    this.props.setVideoState({variable: "play", value: false});
  }

  fullScreenOrHide() {
    this.props.setVideoState({variable: "fullscreen", value: toggleFullScreen()});
  }

  onTimeUpdate() {
    let currentTimesec = Math.round(this.video.currentTime);
    this.props.setVideoState({variable: "time", value: currentTimesec});
  }
  updateVolume(percentage){
    this.props.setVideoState({variable: "volume", value: percentage});
  }
  _onMouseMove(){
    if(!this.hls_buffered){
      return;
    }
    clearTimeout(this.timer);
    this.setState({
      hidePanels: false
    });
    this.timer = setTimeout(function () {
      if(/*this.props.fullscreen && */ this.props.play){
        this.setState({
          hidePanels: true
        });
      }
    }.bind(this), 5000)
  }
  /** Actions/events [END] **/

  renderHeader(){
    return !this.state.hidePanels ? (<div className="video-container__header-container">
      {this.props.data.title}
    </div>) : null;
  }

  renderPanel(){
    return !this.state.hidePanels ? (<div className="controls-container">
      <div className="controls-container__left">
        <PlayButton play={!this.props.play} _onClick={this.playOrStop.bind(this)}/>
        <VolumeButton volume={this.props.volume} updateVolume={this.updateVolume.bind(this)}/>
      </div>
      <div className="controls-container__right">
        <Time value={getTimeFromSeconds(this.props.time)}/>
        <FullScreenButton fullscreen={!this.props.fullscreen} _onClick={this.fullScreenOrHide.bind(this)}/>
      </div>
    </div>) : null;
  }

  renderVideo() {
    return (
      <div className="video-container" draggable="false">
        {this.renderHeader()}
        <div className="video-container__flex">
          <video
            ref={(video) => {
              this.video = video;
            }}
            className="video-container__video"
            width={this.state.width}
            height={this.state.height}
            onEnded={this.videoFinished.bind(this)}
            onTimeUpdate={this.onTimeUpdate.bind(this)}
          >
            Браузер не поддерживает html5 видео.
          </video>
        </div>
        {this.renderPanel()}
      </div>
    )
  }

  render() {
    if(this.props.data == null){
      return null;
    }
    return (
      <div className="player-container">
        {this.renderVideo()}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    play: state.Video.play,
    fullscreen: state.Video.fullscreen,
    time: state.Video.time,
    volume: Math.round(state.Video.volume),
    data: state.Video.data
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getData: () => {dispatch(videoGetData())},
    setVideoState: ({variable, value})=> {
      dispatch(videoAction({variable, value}))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Player)
