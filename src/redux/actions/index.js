import { videoAction, videoGetData } from './Video'

// export actions
export  {
  videoAction,
  videoGetData
}
