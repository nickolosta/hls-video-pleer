import React from 'react'
import axios from "axios"

const videoAction = ({variable, value}) => {
  return {
    type: 'VIDEO_ACTION',
    variable,
    value
  }
};

const videoGetData = () => {
  return function (dispatch) {
    axios.get("http://www.mocky.io/v2/590c8058270000d610b2a8ad")
      .then(function(response) {
        dispatch({
          type: 'VIDEO_SUCCESS_LOAD',
          data: response.data
        })
      }).catch(function (error) {
      dispatch({
        type: 'VIDEO_FAIL_LOAD',
        error: error
      })
    })
  }
};

export  {
  videoAction,
  videoGetData
}
