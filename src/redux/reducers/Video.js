import { deepClone } from "../../lib/Utilities"

const InitState = {
  play: false,
  volume: 100,
  fullscreen: false,
  time: 0,
  data: null
};

const Video = (state = InitState, action) => {
  let newState = deepClone(state);
  switch (action.type) {
    case 'VIDEO_ACTION':
      newState[action.variable] = action.value;
      return newState;
    case 'VIDEO_SUCCESS_LOAD':
      console.log("data", action.data);
      newState.data = action.data;
      return newState;
    default:
      return state
  }
};

export default Video
