import React from 'react';
import {createStore, applyMiddleware} from 'redux'
import { Provider } from 'react-redux'
import reducers from './redux/reducers'
import '../styles/index.scss';
import { Player } from './containers';
import thunk from "redux-thunk"

let store = createStore(reducers, applyMiddleware(thunk));

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Player/>
      </Provider>
    )
  }
}
