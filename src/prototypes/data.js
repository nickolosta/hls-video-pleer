import { uniqueId, deepClone } from "../lib/Utilities"

const data = [
  {
    name: "film 1",
    src: "https://marketplace.canva.com/MACGkF3LOPw/1/0/thumbnail_large/canva-dramatic-black-and-white-film-poster-MACGkF3LOPw.jpg"
  },
  {
    name: "film 2",
    src: "http://www.kino-teatr.ru/movie/poster/117077/83883.jpg"
  },
  {
    name: "film 3",
    src: "http://cs9.pikabu.ru/post_img/2017/02/19/4/1487481861170016714.jpg"
  },
  {
    name: "film 4",
    src: "https://www.film.ru/sites/default/files/movies/posters/5629814-922562.jpg"
  },
  {
    name: "film 5",
    src: "https://www.film.ru/sites/default/files/movies/posters/5629814-922562.jpg"
  },
  {
    name: "film 6",
    src: "https://upload.wikimedia.org/wikipedia/ru/thumb/3/31/Justice_League_film_poster.jpg/324px-Justice_League_film_poster.jpg"
  },
  {
    name: "film 7",
    src: "http://www.youloveit.ru/uploads/posts/2017-03/1490459820_youloveit_ru_liga_spravedlivosti_film_postery02.jpg"
  }
];

export function getDataOffset(limit, offset, dat) {
  return dat.slice(offset,offset+limit);
}

export function getRandomArbitrary(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getFilms(){
  let films = [];
  for(let i=0; i< 1000; i++){
    let dataRow = deepClone(data[getRandomArbitrary(0,data.length-1)]);
    dataRow.id = uniqueId();
    films.push(dataRow);
  }
  return films;
}
